# geolabel-ssno-paper

> Graupner, A.; Nüst, D. _Serverless GEO Labels for the Semantic Sensor Web_. Preprints 2020, 2020020326 (doi: [10.20944/preprints202002.0326.v1](https://doi.org/10.20944/preprints202002.0326.v1)).

The analysis data is published in the GEO-label-java source code repository [github.com/nuest/GEO-label-java](https://github.com/nuest/GEO-label-java), which is archived to Zenodo at [doi:10.5281/zenodo.3673870](https://doi.org/10.5281/zenodo.3673870).
The workflow scripts, plotting functions, and paper sources are published in the repository [gitlab.com/nuest/geolabel-ssno-paper](https://gitlab.com/nuest/geolabel-ssno-paper), which is archived to Zenodo at [doi:10.5281/zenodo.3908399](https://doi.org/10.5281/zenodo.3908399).

## Use figures app on Heroku

The figures app is deployed on Heroku at [**https://geolabel-ssno-paper.herokuapp.com/**](https://geolabel-ssno-paper.herokuapp.com/)

## Rendor manuscript and run figures app locally

_Note_: This requires a version of the `rticles` packages with the LIPIcs template, see [pull request](https://github.com/rstudio/rticles/pull/288).
This dependency is configured in the `DESCRIPTION` file used by `devtools::install()` below.

```r
devtools::install()

rmarkdown::render("paper/paper.Rmd")
# app
```

Or

```bash
make paper.pdf
```

## Run archived computing environment from Zenodo

Note that these instructions utilise an image created for running the figures app on Heroku, in favour of creating a second Dockerfile and Docker image.

1. Download and unzip the contents of https://doi.org/10.5281/zenodo.3908399
2. Import the image:
   
   ```bash
   docker load -i image.tar
   # check with: docker images | grep geolabel-ssno
   ```
3. Run the container with (or a suitable command for your operating system to mount relative to the current path):

   ```bash
   docker run --name glbl-ppr -it -v $(pwd)/geolabel-ssno-paper:/geolabel-ssno-paper geolabel-ssno-paper:published Rscript -e 'rmarkdown::render("/geolabel-ssno-paper/paper/paper.Rmd")'
   ```
   
   This mounts the local directory to a different location than `/app`, because that is where the computing environment is installed, and changes the default process to a shell that we can use, not the default (running the figure app).
   The only command renders the paper.
4. Copy the paper from the container and remove the container:
   
   ```bash
   docker cp glbl-ppr:/geolabel-ssno-paper/paper/paper.pdf geolabel-ssno-paper.pdf
   ```
   
   You should have the file `geolabel-ssno-paper.pdf` in your current directory.

## Creation notes

### Deploy app on Heroku

Based on [these instructions](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4) and using the BuildPack [heroku-docker-r](https://github.com/virtualstaticvoid/heroku-docker-r)
  - plus masking of the variables
  - using container buildpack: `heroku stack:set container --app geolabel-ssno-paper`
  - the dependencies are managed via the `DESCRIPTION` file, see `Dockerfile`

### Zenodo deposit

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3908399.svg)](https://doi.org/10.5281/zenodo.3908399)

The following steps were done to create the Zenodo deposit at https://doi.org/10.5281/zenodo.3908399

1. Create deposition zip archive, using the git version tag
   ```bash
   git clone https://gitlab.com/nuest/geolabel-ssno-paper.git
   
   docker build --tag geolabel-ssno-paper:published --file geolabel-ssno-paper/Dockerfile geolabel-ssno-paper
   docker save -o image.tar geolabel-ssno-paper:published
   docker rmi geolabel-ssno-paper:published
   
   zip -r geolabel-ssno-paper-$(cd geolabel-ssno-paper/; git rev-parse --short HEAD).zip geolabel-ssno-paper image.tar
   
   rm -r image.tar geolabel-ssno-paper
   ```
2. Upload archive to Zenodo
3. Add metadata, using the git version tag again
4. Publish record

To test this, run `unzip geolabel-ssno-paper*.zip` and continue with step 2 of "Run archived computing environment from Zenodo".

### Preprint upload

Set `hideLIPIcs: true` and comment out `relatedversion:` in the YAML header before uploading the PDF to the preprint server.

## License

The texts in this repository are licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

All contained code is licensed under the [Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/).

Copyright 2020 - Daniel Nüst.
