# https://gitlab.com/nuest/geolabel-ssno-paper

# based on https://github.com/virtualstaticvoid/heroku-buildpack-r#shiny-applications
library("here")
library("shiny")

port <- Sys.getenv("PORT", unset = 8000)

shiny::runApp(
  appDir = here::here("inst/"),
  host = "0.0.0.0",
  port = as.numeric(port)
)
