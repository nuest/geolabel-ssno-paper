default: edit

paper: paper/paper.Rmd
	Rscript -e 'rmarkdown::render("$<")'

paper_container: paper/paper.Rmd
	docker build --tag geolabel-ssno .
	docker run --name glbl-ppr-render -it -v $(shell pwd):/geolabel-ssno-paper geolabel-ssno Rscript -e 'rmarkdown::render("/geolabel-ssno-paper/paper/paper.Rmd")'
	docker cp glbl-ppr-render:/geolabel-ssno-paper/paper/paper.pdf geolabel-ssno-paper.pdf
	docker rm glbl-ppr-render

app: 
	Rscript run.R
	
app_container:
	docker build --tag geolabel-ssno-heroku .
	docker run --rm -it -p 8080:8080 geolabel-ssno-heroku

edit:
	repo2docker --editable .
