theme_base_size <- 10
colours <- c("#7C8084", "#FDC700")

#' Plot the elapsed times
#' 
#' Based on plotting functions from package `loadtest`, see https://github.com/tmobile/loadtest/blob/master/R/plots.R
#' 
#' Code copied here so colors, titles etc. could be adjusted.
#' 
#' @param results a data.frame with the results
#' 
#' @import ggplot2
#' @importFrom stats median
#' 
#' @export
plot_elapsed_times <- function(results){
  ggplot2::ggplot(results,
                  ggplot2::aes(x = .data$time_since_start, y = .data$elapsed, color = .data$request_status)) +
    ggplot2::geom_point() +
    ggplot2::labs(x = "Time since start (seconds)",
                  y = "Time to complete request (milliseconds)",
                  color = "Request status",
                  title = "Time to complete request",
                  subtitle = "over duration of the test",
                  caption = paste0("Horizontal line is the median response (", stats::median(results$elapsed), ")")) +
    ggplot2::theme_minimal(base_size = theme_base_size) +
    ggplot2::scale_color_manual(values = colours, drop = FALSE) +
    ggplot2::theme(legend.position = "bottom") +
    ggplot2::scale_y_continuous(limits = c(0,NA)) +
    ggplot2::geom_hline(yintercept = stats::median(results$elapsed))
}

#' Plot the elapsed times in form of a histogram
#' 
#' 
#' Based on plotting functions from package loadtest, see https://github.com/tmobile/loadtest/blob/master/R/plots.R
#' 
#' Code copied here so colors, titles etc. could be adjusted.
#' 
#' @param results a data.frame with the results
#' @param binwidth the histogram bin width
#' 
#' @import ggplot2
#' 
#' @export
plot_elapsed_times_histogram <- function(results, binwidth = 250){
  ggplot2::ggplot(results, ggplot2::aes(x = .data$elapsed, fill = .data$request_status)) +
    ggplot2::geom_histogram(binwidth = binwidth, color = colours[1]) +
    ggplot2::scale_x_continuous(breaks=seq(0, max(results[["elapsed"]]), binwidth)) +
    ggplot2::theme_minimal(base_size = theme_base_size) +
    ggplot2::scale_fill_manual(values = colours, drop = FALSE) +
    ggplot2::labs(x = "Time to complete response (milliseconds)",
                  fill = "Request status",
                  title = "Distribution of response times",
                  subtitle = "for completion of responses") +
    ggplot2::theme(legend.position = "bottom",
                   axis.text.x = element_text(angle = 90,
                                              hjust = 1,
                                              size = 6))
}

#' Plot all scenario plots
#' 
#' @param scenario the scenario data to load
#' @param title the main title for the combined figure
#' 
#' @export
#' @import patchwork
plot_scenario <- function(scenario, title) {
  ( plot_elapsed_times(scenario) | plot_elapsed_times_histogram(scenario) ) +
    patchwork::plot_annotation(title = title)
}