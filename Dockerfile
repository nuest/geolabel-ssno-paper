# Started from https://github.com/virtualstaticvoid/heroku-docker-r#shiny-applications
FROM virtualstaticvoid/heroku-docker-r:shiny

# Dependencies not needed for app, but for double usage of container for the rendering of the manuscript
RUN apt-get update && apt-get install -y \
  pandoc

# Install packages dependencies, force installation of rticles to specific revision
RUN Rscript -e "install.packages('remotes')" && \
  Rscript -e "remotes::install_gitlab('nuest/geolabel-ssno-paper@main', \
    upgrade = 'never', build_manual = FALSE, build_vignettes = FALSE)" && \
  Rscript -e "remotes::install_github('nuest/rticles@103fe454cd5df18965c8d37755c45a8755f6fa96')"

# Install afterwards, otherwise XeLaTex is not found
RUN Rscript -e "tinytex::install_tinytex()"

# Clone once to retrieve data files, speeds up instantiation on Heroku
WORKDIR /app
RUN git clone https://github.com/nuest/GEO-label-java.git

ENV PORT=8080

LABEL maintainer="Daniel Nüst <daniel.nuest@uni-muenster.de>" \
  org.opencontainers.image.authors="Daniel Nüst, Anika Graupner" \
  org.opencontainers.image.url="https://gitlab.com/nuest/geolabel-ssno-paper/" \
  org.opencontainers.image.documentation="https://www.preprints.org/manuscript/202002.0326" \
  org.opencontainers.image.description="Reproducible workflow image for the paper 'Serverless GEO Labels for the Semantic Sensor Web'" \
  org.opencontainers.image.version="preprint.v2"

CMD ["/usr/bin/R", "--no-save", "--gui-none", "-f /app/run.R"]

# Test locally
# docker build --tag geolabel-ssno-heroku .
# docker run --rm -it geolabel-ssno-heroku /bin/bash
# docker run --rm -it -p 8080:8080 geolabel-ssno-heroku
